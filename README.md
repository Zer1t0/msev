# Microsoft Email Verifier (msev)

msev is based on the idea of [o365creeper](https://github.com/LMGsec/o365creeper) of checking emails without login but improved for usability.

## Usage

Just provide emails or file with email per line and it will print the valid ones:
```
$ echo admin@contoso.com | msev
admin@contoso.com
$ msev /tmp/emails.txt
admin@contoso.com
```

## Installation

```
pip install git+https://gitlab.com/Zer1t0/msev
```

